const express = require('express');
const app = express();
const PORT = 5000;
const fileUpload = require('express-fileupload');
global.appRoot = __dirname;
var cors = require('cors');
const path = require('path');
require('dotenv').config()

app.use(express.static(path.join(__dirname, '/build')));
app.use(express.static('public'));

app.use(cors())

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/blog', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log('mongodb connected')
    })
    .catch(err => {
        console.log(err);
    });

const bodyParser = require('body-parser');
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

const morgan = require('morgan');
app.use(morgan('dev'));

app.use(fileUpload({
    limits: { fileSize: 50 * 1024 * 1024 },
}));
app.get('', (req, res) => {
    res.sendFile(path.join(__dirname, '/build/index.html'));
})

const routes = require('./server/routes/routes');
app.use('/api', routes);


app.listen(PORT, () => {
    console.log('Serve started at port:', PORT);
})