const post = require('../models/post');
// const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
const User = require('../models/user');

// get all posts
const getPosts = async (req, res) => {
    const { userId } = req.query; // api/?userId="id"
    let filter = {};
    if (userId) filter.createdBy = userId;

    try {
        let data = await post.find(filter)
            .populate({ path: 'createdBy', select: 'name email', model: User })
            .sort({ createdAt: -1 });

        // let data = await post.aggregate([
        //     // {
        //     //     $match: { _id: _id }
        //     // },
        //     {
        //         $lookup:
        //         {
        //             from: "users",
        //             localField: "createdBy",
        //             foreignField: "_id",
        //             as: "author"
        //         }
        //     },
        //     {
        //         $project: {
        //             'author.password': 0,
        //             'author._id': 0
        //         }
        //     }
        // ])

        if (data.length === 0) {
            return res.status(404).json({
                message: 'No data found.'
            })
        }

        let posts = data.map(post => {
            post.body = post.body.substring(0, 10);
            return post;
        })

        res.status(200).json(data);
    } catch (err) {
        console.log(err)
        res.status(500).json({
            message: err.message
        })
    };

}

// get single post by id
const getPost = async (req, res) => {
    let { _id } = req.params;

    // _id = mongoose.Types.ObjectId(_id);

    try {
        let result = await post.findOne({ _id });

        if (result === null) {
            return res.status(400).json({
                message: 'Invalid ID.'
            })
        }

        res.status(200).json(result);
    } catch (error) {
        res.status(500).json({
            message: error.message
        });
    }
}

// create new post
const createPost = async (req, res) => {
    console.log(req.userData)
    let data = req.body;

    // Validation (sasto)
    // let { title, body } = data;
    // if (!title) {
    //     return res.status(422).json({
    //         message: 'title is required'
    //     })
    // }
    const schema = Joi.object({
        title: Joi.string().max(250).required(),
        body: Joi.string().min(3).max(20000),
        createdAt: Joi.any(),
        createdBy: Joi.any()
    })

    data.createdAt = Date.now();
    data.createdBy = req.userData._id;

    let newPost = new post(data);

    try {
        const validationError = schema.validate(req.body, { abortEarly: false });
        if (validationError && validationError.error) {
            console.log(validationError.error.details[0].message);
            // Using for loop
            // let message = [];
            // for (let i = 0; i < validationError.error.details.length; i++) {
            //     message.push(validationError.error.details[i].message);
            // }

            // Using map
            let message = validationError.error.details.map(data => {
                return data.message;
            })
            return res.status(422).json({
                message
            });
        }

        let result = await newPost.save(data);

        res.status(201).json({
            status: 'OK',
            message: 'success',
            newPost: result
        })
    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }

    // OLD Way
    // newPost.save(data)
    //     .then(result => {
    //         console.log(result);

    //         res.status(201).json({
    //             status: 'OK',
    //             message: 'success',
    //             newPost: result
    //         })
    //     })
    //     .catch(error => {
    //         console.log(error);
    //         res.status(500).json({
    //             message: error.message
    //         })
    //     })
}

const updatePost = async (req, res) => {
    let { _id: postId } = req.params;
    // let postId = _id;

    try {
        // sasto tarika
        // let post = await post.find({ _id });
        // //  post.createdBy === token id

        // if (req.userData._id !== post.createdBy) {
        //     return res.status(401).json({
        //         message: 'Auth failed'
        //     })
        // }

        let result = await post.updateOne(
            {
                _id: postId,
                createdBy: req.userData._id
            },
            {
                $set: req.body
            },
            // {
            //     upsert: true
            // }
        );

        if (result.nModified === 0) {
            return res.status(400).json({
                message: "Failed to update data"
            })
        }

        res.status(200).json({
            status: 'OK'
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: error.message
        })
    }
}

const deletePost = async (req, res) => {
    let { _id } = req.params;

    try {
        let result = await post.deleteOne({ _id });

        let { deletedCount } = result;
        if (deletedCount === 0) {
            return res.status(400).json({
                message: "Failed to delete data"
            })
        }

        res.status(200).json({
            status: 'OK'
        })
    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

const searchPost = async (req, res) => {
    let { text } = req.params;
    text = new RegExp(text, 'i');

    try {
        let response = await post.find({
            $or: [
                { title: text },
                { body: text }
            ]
        });

        //not found logic

        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

module.exports = {
    getPosts,
    getPost,
    createPost,
    updatePost,
    deletePost,
    searchPost
}


// const abc = 'abc';
// const def = 'def';

// {
//     abc: abc,
//         def: def
// }

// {
//     abc,
//     def
// }
