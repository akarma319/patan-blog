const bcrypt = require('bcrypt');
const SALT_ROUNDS = 10;
const User = require('../models/user');
const jwt = require('jsonwebtoken');
// const SECRET_KEY = 'This is my private key sdfsadfsdfdas322234324';
const { SECRET_KEY } = process.env;

const login = async (req, res, next) => {
    // take data from body
    let { email, password } = req.body;

    // validation(HomeWork)

    // sanitize 
    email = email.toLowerCase();

    // matching email to db
    let currentUser = await User.findOne({ email });

    // if not found throw error
    // user not exists
    if (currentUser === null) {
        return res.status(401).json({
            message: 'Auth failed'
        })
    }

    // if found match password
    let comparePassword = bcrypt.compareSync(password, currentUser.password);

    if (!comparePassword) {
        return res.status(401).json({
            message: 'Invalid password'
        })
    }

    console.log(SECRET_KEY)
    // generate token and res
    let { _id, name } = currentUser;
    let token = jwt.sign({
        _id,
        email,
        name
    }, SECRET_KEY, { expiresIn: '48h' });

    res.status(200).json({
        token
    })
}

const signup = async (req, res, next) => {
    let { name, email, password, confirmPassword } = req.body;

    // validation logic

    // sanitize
    // aabhaskarma@gmail.com
    // AabhasKarma@Gmail.Com
    // aabhas.karma@gmail.com
    // aabhas.karma+5@gmail.com
    email = email.toLowerCase();

    // checking for duplicate email
    let oldUser = await User.find({ email });

    // email already exists
    if (oldUser.length > 0) {
        return res.status(422).json({
            message: 'Email already Exists'
        })
    }

    // hashing user password
    let hash = bcrypt.hashSync(password, SALT_ROUNDS);

    // saving to DB
    let newUser = new User({
        name,
        email,
        password: hash
    });

    try {
        let result = await newUser.save();

        res.status(201).json({
            status: 'OK',
            newUser: result
        });
    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }

}

module.exports = {
    login,
    signup
}
