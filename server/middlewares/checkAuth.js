const jwt = require('jsonwebtoken');
// const SECRET_KEY = 'This is my private key sdfsadfsdfdas322234324';
const { SECRET_KEY } = process.env;

const checkAuth = (req, res, next) => {
    let token = req.headers.authorization.split(" ")[1];  // bearer asdasdasd
    try {
        var decoded = jwt.verify(token, SECRET_KEY);

        req.userData = decoded;

        next();
    } catch (err) {
        res.status(401).json({
            message: 'Auth failed'
        })
    }
}


module.exports = checkAuth;