const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const postSchema = new Schema({
    title: { type: String, required: true },
    body: String,
    createdAt: Date,
    createdBy: mongoose.Schema.Types.ObjectId,
    imageUrl: String
});

module.exports = mongoose.model('post', postSchema);


// ['mime1', mime2, ...].includes('myFile.type')