const express = require('express');
const router = express.Router();
const checkAuth = require('../middlewares/checkAuth');

const { getPosts,
    getPost,
    createPost,
    updatePost,
    deletePost,
    searchPost
} = require('../controllers/controllers');

router.get('/', getPosts);
router.get('/:_id', getPost);
router.get('/search/:text', searchPost);
router.post('/', checkAuth, createPost);
router.patch('/:_id', checkAuth, updatePost);
router.delete('/:_id', checkAuth, deletePost);

const { login,
    signup
} = require('../controllers/user');

router.post('/login', login);
router.post('/signup', signup);

router.post('/fileupload', (req, res, next) => {
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).json({ message: 'No files were uploaded.' });
    }


    let files = req.files;

    //is exceed file limit
    if (files.myFile.truncated === true) {
        return res.status(400).json({
            message: 'file size too large (5mb)'
        })
    }

    // mimetype
    if (files.myFile.mimetype !== 'image/jpeg') {
        return res.status(400).json({
            message: 'file type not allowed(jpeg)'
        })
    }

    // changing file name
    files.myFile.name = `${Date.now()}-${files.myFile.name}`;

    console.log(appRoot);
    let filePath = `${appRoot}/public/${files.myFile.name}`

    files.myFile.mv(`/public/${files.myFile.name}`, function (err) {
        if (err)
            return res.status(500).send(err);

        res.send('File uploaded!');
    });
    console.log(req.files);
})

module.exports = router;
